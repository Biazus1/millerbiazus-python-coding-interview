from rest_framework.exceptions import MethodNotAllowed
from rest_framework import viewsets

from accounts.models import Student
from accounts.api.serializers import StudentSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    def destroy(self):
        raise MethodNotAllowed('Delete is not allowed for this object.')
