from rest_framework import routers

from accounts.api.views import StudentViewSet


router = routers.SimpleRouter()
router.register(r'students', StudentViewSet)
urlpatterns = router.urls
