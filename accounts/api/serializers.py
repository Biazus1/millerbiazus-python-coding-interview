from rest_framework import serializers

from accounts.models import Student


class StudentSerializer(serializers.ModelSerializer):
    # username = serializers.

    class Meta:
        model = Student
        fields = ['id', 'enrollment_completed']
